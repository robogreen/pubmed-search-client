# Pubmed Search

## About

This app has been bootstrapped using the create-react-app script. 
It has been broken down into reusable components and hooks, for example, when implementing the reading list feature, the PubTable component and the usePubmedDetail hook can be used for displaying
results. 

## Approach

I've attempted (within the allocated time) to produce a simple application that demonstrates how I would structure the code, build out the requested features etc. I have demonsrated how I would produce reusable hooks that can build upon each other to provide a simple means of interacting with the back end services. Given more time I would have liked to implement some tests for the hooks and implented typing such that I can be more confident in the data structre coming out of the hooks, as well as robust error handling for when the servers are not available or rate limits are exceeded. 

Task 1 is complete however Task 2 would require more time to implment the add/remove button for the reading list, I have attempted to show how I would achieve this by producing the web service, a hook to read the reading list from it and making the display of publications component reusable such that I can display the results there. 

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Improvements to make

There are a number of improvements to be made, such as:
- [] Test coverage, I want to implement functional tests for each of the hooks
- [] Typing or PropTypes, I would like to remove the fragility of the code and risk of any runtime errors by migrating the files to Typescript. 
- [] Error/loading handling, provide a cleaner means of showing these states to the user. 
- [] Given more time I would like to implement the add/remove button for the reading list that calls the pubmed-backend service. 
- [] Styling, in the name of expediency I've done very little to style the app, favouring inline styles over any higher level, reusable stylesheet. 


