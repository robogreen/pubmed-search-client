import './App.css';
import '@fontsource/roboto';
import * as React from 'react';
import Results from './components/results';
import { SearchBar } from './components/searchBar';
import { Typography } from '@material-ui/core';
import ReadingList from './components/readingList';

// Simple app to search Pubmed, display results to a single user
// and allow them to populate a reading list.
function App() {

  const [searchTerm, setSearchTerm] = React.useState("")

  return (
    <div className="App">
        <Typography variant="h3">
          Search PubMed
        </Typography>
      <SearchBar onSubmit={setSearchTerm} />
      <Results searchTerm={searchTerm} />
      <ReadingList />
    </div>
  );
}

export default App;



