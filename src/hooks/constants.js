import axios from 'axios';
import rateLimit from 'axios-rate-limit';

// Base URL for API
export const BASE_URL = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils" 
// Base URL for Paper on Pubmed site
export const BASE_PAPER_URL = "https://pubmed.ncbi.nlm.nih.gov"
// NOTE API Key is stored here, which definitely is not a good idea
// but placed here fore expediency sake whilst there is no requirement
// for security on the application.
export const DEFAULT_PARAMS = {
    db: "pubmed",
    retmax: 20,
    retmode: "json",
    api_key: "47a6aa577d3fa296b6ddb8d23aa3b4332f09"
}

// Pubmed implements rate limitting of 3rps without key, 10rps with key. 
export const HTTP = rateLimit(axios.create(), { maxRequests: 10, perMilliseconds: 1000})

// Base URL for reading list service
export const BASE_READING_LIST_URL = "http://localhost:8000/readinglist"