import axios from 'axios';
import * as React from 'react';
import { BASE_READING_LIST_URL } from './constants';
import { usePubmedDetail } from './usePubmedDetail';

function useReadingListData() {

    const [resultIDs, setResultIDs] = React.useState([])
    const [error, setError] = React.useState(null)

    const {results, setDetailIDs} = usePubmedDetail()
    
    React.useEffect(() => {
        axios.get(BASE_READING_LIST_URL)
        .then((response) => {
            setResultIDs(response.data.id)
        }).catch((error) => {
            console.log(error)
            setError(true)
        })
    },[]) // load on first call
    
    React.useEffect(() => {
        if (resultIDs) {
            setDetailIDs(resultIDs)
        }
    }, [resultIDs, setDetailIDs])

    return {
        readingListResults: results,
        readingListError: error,
    }
}

export {useReadingListData}