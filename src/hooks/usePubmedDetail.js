import * as React from 'react';
import { BASE_PAPER_URL, BASE_URL, DEFAULT_PARAMS, HTTP } from './constants';


function usePubmedDetail() {

    const [resultIDs, setResultIDs] = React.useState([])
    const [error, setError] = React.useState(null)
    const [results, setResults] = React.useState([])
    
    React.useEffect(() => {

        let rows = []

        if (resultIDs === undefined) {
            setResults(rows)
        } else {
            // Create string of result uids for query param
            const ids = resultIDs.join(",")
            HTTP.get(
                `${BASE_URL}/esummary.fcgi`,
                { params: {
                    ...DEFAULT_PARAMS,
                    id: ids
                }})
            .then((response) => {
                // Check results were found (no results returns 200 HTTP response code)
                if (response.data.result !== undefined) {
                    const uids = response.data.result.uids
                    rows = uids.map(element => {
                        let row = response.data.result[element]
                        // provide id field, useful if results are displayed in material data grids
                        row['id'] = row['uid']
                        // populate field for paper URL on Pubmed site
                        row['pubmedURL'] = `${BASE_PAPER_URL}/${row['uid']}`
                        return row
                    })
                }
            setResults(rows)
        }).catch((error) => {
            console.log(error)
            setError(true)
        })
    }

        
    }, [resultIDs]) // Hook executes when the results ids are updated

    return {
        detailError: error,
        results,
        setDetailIDs: setResultIDs
    }

}

export {usePubmedDetail};
