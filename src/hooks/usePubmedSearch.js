import * as React from 'react';
import { BASE_URL, DEFAULT_PARAMS, HTTP } from './constants';
import { usePubmedDetail } from './usePubmedDetail';

function usePubMedSearch() {

    const [count, setCount] = React.useState(0)
    const [cursor, setCursor] = React.useState(0)
    const [error, setError] = React.useState(null)
    const [searchTerm, setSearchTerm] = React.useState("") 

    const {results, setDetailIDs} = usePubmedDetail()

    // Reset the results and counter when the search term is changed
    const updateSearchTerm = (newTerm) => {
        setCount(0)
        setDetailIDs([])
        setCursor(0)
        setError(null)
        setSearchTerm(newTerm)
    }

    React.useEffect(() => {

        // if there is no search term, provide no results
        if (!searchTerm) {
            setDetailIDs([])
            setCount(0)
        } else {
            // Let hook caller know results are loading
            // On success, populate the result ids and results count
            HTTP.get(
                `${BASE_URL}/esearch.fcgi`,
                {params: {
                    ...DEFAULT_PARAMS,
                    term: searchTerm,
                    retstart: cursor
                }}
                )
            .then((response) => {
                setDetailIDs(response.data.esearchresult.idlist)
                setCount(response.data.esearchresult.count)
            }).catch((error) => {
                console.log(error)
                setError(true)
            })
        }
        
    }, [cursor, searchTerm, setDetailIDs]) // Execute every time the page or term updates
    
    return {
        resultsCount: count,
        setSearchCursor: setCursor,
        searchError: error,
        searchResults: results,
        setSearchTerm: updateSearchTerm
    }

}

export {usePubMedSearch};
