import * as React from 'react';
import { usePubMedSearch } from '../hooks/usePubmedSearch';
import PubTable from './pubTable';

function Results({ searchTerm }) {
  
    const [page, setPage] = React.useState(0);
    const [pageLength] = React.useState(20);

    const {
        resultsCount,
        setSearchCursor,
        searchError, 
        searchResults, 
        setSearchTerm
    } = usePubMedSearch()

    // Udate the search term provided to the search data hook
    // whenever the prop is changed.
    // TODO does this lead to strange behaviour for a user
    // when selecting "search" without changing the term?
    React.useEffect(() => {
        setSearchTerm(searchTerm)
    },[searchTerm, setSearchTerm])

    // Update the page given to the search data hook
    // When a new page event is fired
    const handlePageChange = (newPage) => {
        let newCursor = newPage * pageLength
        setSearchCursor(newCursor)
        setPage(newPage)
    }

    return (
        <PubTable
            title="Results"
            publications={searchResults}
            handlePageChange={handlePageChange}
            pageSize={pageLength}
            pubCount={resultsCount}
            page={page}
            hasError={searchError}
            paginationMode="server"
        />
    );
}

export default Results;
