import * as React from 'react';
import { TextField } from '@material-ui/core';
import { Button } from '@material-ui/core';

function SearchBar({onSubmit}) {

    const [searchTerm, setSearchTerm] = React.useState("")

    const handleInputChange = (event) => {setSearchTerm(event.target.value)}

    return (
            <div>
                 {/* TODO remove inline styles in favour of style sheet or other theming */}
                <div style={{ paddingBottom: 20 }}>
                    <TextField label="Search" variant="filled" onChange={handleInputChange}/>
                </div>
                <div>
                    {/* Make sure search term is available to callback onClick */}
                    <Button variant="contained" color="primary" onClick={() => onSubmit(searchTerm)}>
                        Search
                    </Button>
                </div>
            </div>
    )

}

export {SearchBar};