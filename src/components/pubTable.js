import * as React from 'react';
import { DataGrid } from '@material-ui/data-grid';
import Typography from '@material-ui/core/Typography';
import { Button } from '@material-ui/core';


function PubTable({ title, publications, handlePageChange, pageSize, pubCount, page, hasError, paginationMode }) {
  
    // TODO add renderCell for button
    // TODO create callback for PUT to add Pub to reading list
    const ReadingListButton = ({id, inList}) => {
        return (
            <Button variant="contained" color="primary">
                {inList? "Add" : "Remove" }
            </Button>
        )
    }

    return (
        // Inline styles used for the sake of expediency
        <div style={{width: "100%", paddingTop: 20, paddingBottom: 20}}>
            <Typography variant="h4" gutterBottom align="left">
                {title}
            </Typography>
            <DataGrid
                // Provide string of authors, note that this could be improved
                // by providing a render method to the collumn definition.
                // E.g. a line per author within the cell. 
                rows={publications.map((pub) => ({
                    ...pub,
                    authoredBy: pub.authors.map((author) => author.name).join(", ")
                }))}
                columns={[
                    { 
                        field: 'title',
                        headerName: 'Title', 
                        minWidth: 400, flex: 3,
                        // Render the title as a link to publication on Pubmed
                        renderCell: (params) => (
                            <a href={params.row.pubmedURL} target="_blank" rel="noreferrer">
                                {params.value}
                            </a>
                        )},
                    { field: 'authoredBy', headerName: 'Author(s)', minWidth: 200, flex: 3},
                    { field: 'epubdate', headerName: 'Publish Date', minWidth: 100, flex: 2},
                    { field: 'fulljournalname', headerName: 'Journal', minWidth: 100, flex: 3},
                    // Make all columns not filterable or sortable, requires more complex server
                    // interaction to enable expected behaviour from user
                    // i.e. filter or sort ALL results not just page. 
                    ].map((column) => ({
                        ...column,
                        sortable: false,
                        filterable: false})
                        )
                    }
                pagination
                pageSize={pageSize}
                rowCount={pubCount}
                // Enable server side pagination
                paginationMode={paginationMode}
                onPageChange={handlePageChange}
                page={page}
                autoHeight={true}
                error={hasError}
                disableSelectionOnClick
            />
        </div>
    );
}

export default PubTable;
