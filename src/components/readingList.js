import * as React from 'react';
import { useReadingListData } from '../hooks/useReadingListData';
import PubTable from './pubTable';

function ReadingList({pub_ids}) {
  
    const {readingListResults, readingListError} = useReadingListData()

    return (
        <PubTable
            title="Reading List"
            publications={readingListResults}
            paginationMode="client"
            hasError={readingListError}
            />
    );
}

export default ReadingList;
